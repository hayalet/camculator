package core;

import static com.googlecode.javacv.cpp.opencv_highgui.CV_CAP_ANY;
import static com.googlecode.javacv.cpp.opencv_highgui.cvCreateCameraCapture;
import static com.googlecode.javacv.cpp.opencv_highgui.cvQueryFrame;
import static com.googlecode.javacv.cpp.opencv_highgui.cvReleaseCapture;

import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import process.ImageProcessor;

import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_highgui.CvCapture;

public class Webcam extends JFrame {

    private JPanel contentPane;

    /**
     * Image processor
     */
    private ImageProcessor imgp;

    /**
     * Thread for video capture
     */
    private Capture capture;

    /**
     * Real time capture
     */
    private boolean realTime;

    /**
     * Create the frame.
     */
    public Webcam() {
	imgp = new ImageProcessor();
	capture = null;
	realTime = true;

	setTitle("Camculator | Webcam");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(600, 150, 320, 240);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(new BorderLayout(0, 0));
	setContentPane(contentPane);
    }

    /**
     * Get image processor
     * 
     * @return
     */
    public ImageProcessor getImgp() {
	return this.imgp;
    }

    /**
     * Set capturing video
     */
    public void setCapture(boolean flag) {
	if (flag) {
	    capture = new Capture();
	    capture.start();
	} else {
	    if (capture != null) {
		capture = null;
	    }
	}
    }

    /**
     * Check whether camera is capturing
     * 
     * @return
     */
    public boolean isCapturing() {
	return capture != null;
    }

    /**
     * Set real time capture
     * 
     * @param flag
     */
    public void setRealTime(boolean flag) {
	this.realTime = flag;
    }

    /**
     * Is real time capture?
     * 
     * @return
     */
    public boolean isRealTime() {
	return this.realTime;
    }

    /**
     * Thread capturing video
     */
    class Capture extends Thread {
	public void run() {
	    try {
		CvCapture handle = cvCreateCameraCapture(CV_CAP_ANY);

		IplImage img = cvQueryFrame(handle);
		setSize(img.width(), img.height());

		boolean processed = false;
		while ((capture != null)) {
		    if (realTime || img == null) {
			processed = false;
			img = cvQueryFrame(handle);
			if (img == null) {
			    break;
			}
		    }
		    
		    imgp.init(img);

		    if (!processed) {
			imgp.reset();
			imgp.process();
			processed = true;
		    }

		    if (realTime) {
			imgp.drawAll();

			App.options.updateResult();
		    } else {
			imgp.drawCurrentContour();
			imgp.drawCurrentEquation();
		    }

		    Graphics g = getGraphics();
		    switch (App.options.getPreviewMode()) {
		    case Input:
			g.drawImage(imgp.getInput(), 0, 0, null);
			break;
		    case Processed:
			g.drawImage(imgp.getProcessed(), 0, 0, null);
			break;
		    case Contured:
			g.drawImage(imgp.getContured(), 0, 0, null);
			break;
		    case Output:
			g.drawImage(imgp.getOutput(), 0, 0, null);
			break;
		    }
		}

		cvReleaseCapture(handle);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }
}
