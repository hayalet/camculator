package core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.script.ScriptException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import process.Contour;
import process.Equation;
import process.ImageProcessor;
import process.Symbol;

public class Options extends JFrame {

    public enum Preview {
	Input, Processed, Contured, Output
    };

    private JPanel contentPane;
    private JButton btnLearning;
    private JButton btnCapturing;
    private JButton btnAdd;

    private JSpinner spEquation;
    private JSpinner spContour;
    private JTextField fldCharacter;

    private JTextPane txtInfo;
    private JTextPane txtResult;

    private JComboBox cbPreview;
    private JButton btnLoadDictionary;
    private JButton btnSaveDictionary;
    private JButton btnClearDictionary;
    private JButton btnRemove;

    /**
     * Create the frame.
     */
    public Options() {
	setTitle("Camculator | Options");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(100, 100, 460, 408);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);

	JLabel lblLearning = new JLabel("Learn mode");

	btnLearning = new JButton("Enable");
	btnLearning.setEnabled(false);
	btnLearning.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {

		btnAdd.setEnabled(false);
		btnRemove.setEnabled(false);

		fldCharacter.setEnabled(false);
		fldCharacter.setText("");

		spEquation.setEnabled(false);
		spContour.setEnabled(false);

		// Toggle camera mode
		if (App.webcam.isRealTime()) {
		    App.webcam.setRealTime(false);

		    btnLearning.setText("Disable");
		    btnCapturing.setEnabled(false);

		    // Control image processor
		    ImageProcessor imgp = App.webcam.getImgp();
		    Equation[] equations = imgp.getEquations();

		    // Clear old chosen equation and contour
		    imgp.setCurrentEquation(null);
		    imgp.setCurrentContour(null);

		    if (imgp.getEquations().length > 0) {
			spEquation.setEnabled(true);

			// Set first as active
			Equation equation = equations[0];
			Contour[] contours = equation.getContours();

			imgp.setCurrentEquation(equation);
			spEquation.setModel(new SpinnerNumberModel(0, 0, equations.length - 1, 1));

			if (contours.length > 0) {
			    spContour.setEnabled(true);
			    fldCharacter.setEnabled(true);

			    // Set first as active
			    Contour contour = contours[0];
			    imgp.setCurrentContour(contour);
			    spContour.setModel(new SpinnerNumberModel(0, 0, contours.length - 1, 1));
			}
		    }
		} else {
		    App.webcam.setRealTime(true);

		    btnLearning.setText("Enable");
		    btnCapturing.setEnabled(true);
		}

		updateInfo();
	    }
	});

	JLabel lblCapturing = new JLabel("Capturing");

	btnCapturing = new JButton("Start");
	btnCapturing.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		updateInfo();

		if (App.webcam.isVisible()) {
		    App.webcam.setCapture(false);
		    App.webcam.setVisible(false);

		    btnCapturing.setText("Start");
		    btnLearning.setEnabled(false);

		} else {
		    App.webcam.setVisible(true);
		    App.webcam.setCapture(true);

		    btnCapturing.setText("Stop");
		    btnLearning.setEnabled(true);
		}
	    }
	});

	JLabel lblEquation = new JLabel("Equation");

	spEquation = new JSpinner();
	spEquation.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent arg0) {
		ImageProcessor imgp = App.webcam.getImgp();

		// Get current equation
		SpinnerNumberModel model = (SpinnerNumberModel) spEquation.getModel();
		Equation equation = imgp.getEquations()[model.getNumber().intValue()];

		// Set equation to processor
		imgp.setCurrentEquation(equation);

		// Set first contour in chosen equation
		imgp.setCurrentContour(equation.getContours()[0]);

		// Reset contour to first
		spContour.setModel(new SpinnerNumberModel(0, 0, equation.getContours().length - 1, 1));
	    }
	});
	spEquation.setEnabled(false);

	JLabel lblContour = new JLabel("Contour");

	spContour = new JSpinner();
	spContour.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent arg0) {
		ImageProcessor imgp = App.webcam.getImgp();
		SpinnerNumberModel model = (SpinnerNumberModel) spContour.getModel();
		imgp.setCurrentContour(imgp.getCurrentEquation().getContours()[model.getNumber().intValue()]);
	    }
	});
	spContour.setEnabled(false);

	JLabel lblCharacter = new JLabel("Character");

	fldCharacter = new JTextField();
	fldCharacter.getDocument().addDocumentListener(new DocumentListener() {
	    public void changedUpdate(DocumentEvent e) {
		check();
	    }

	    public void removeUpdate(DocumentEvent e) {
		check();
	    }

	    public void insertUpdate(DocumentEvent e) {
		check();
	    }

	    public void check() {
		ImageProcessor imgp = App.webcam.getImgp();
		String character = fldCharacter.getText();
		boolean has = imgp.getDictionary().has(character);

		if (!character.isEmpty()) {
		    btnAdd.setEnabled(!has);
		    btnRemove.setEnabled(has);
		} else {
		    btnAdd.setEnabled(false);
		    btnRemove.setEnabled(false);
		}
	    }
	});
	fldCharacter.setEnabled(false);
	fldCharacter.setColumns(10);

	JLabel lblInfo = new JLabel("Info");

	txtInfo = new JTextPane();
	txtInfo.setEditable(false);

	btnAdd = new JButton("Add");
	btnAdd.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		ImageProcessor imgp = App.webcam.getImgp();
		imgp.getDictionary().learn(imgp.getCurrentContour(), fldCharacter.getText());

		Object next = spContour.getNextValue();
		if (next != null) {
		    spContour.setValue(next);
		    fldCharacter.setText("");
		    fldCharacter.requestFocus();
		}

		fldCharacter.setText("");
		btnAdd.setEnabled(false);

		btnClearDictionary.setEnabled(imgp.getDictionary().getSymbols().length > 0);

		updateInfo();
	    }
	});
	btnAdd.setEnabled(false);

	JLabel lblResult = new JLabel("Result");

	txtResult = new JTextPane();
	txtResult.setEditable(false);

	JLabel lblPreview = new JLabel("Preview");

	cbPreview = new JComboBox();
	cbPreview.setModel(new DefaultComboBoxModel(Preview.values()));
	cbPreview.setSelectedIndex(3);

	btnLoadDictionary = new JButton("Load");
	btnLoadDictionary.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		int reply = JOptionPane.showConfirmDialog(null,
			"Are you sure to load previosly saved symbols into dictionary?", "Dictionary",
			JOptionPane.YES_NO_OPTION);

		if (reply == JOptionPane.YES_OPTION) {
		    ImageProcessor imgp = App.webcam.getImgp();
		    imgp.getDictionary().loadFromFile("dictionary.dat");

		    btnClearDictionary.setEnabled(imgp.getDictionary().getSymbols().length > 0);

		    updateInfo();
		}
	    }
	});

	btnSaveDictionary = new JButton("Save");
	btnSaveDictionary.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int reply = JOptionPane.showConfirmDialog(null,
			"Are you sure want to save symbols? Existing data will be overwritten!", "Dictionary",
			JOptionPane.YES_NO_OPTION);

		if (reply == JOptionPane.YES_OPTION) {
		    ImageProcessor imgp = App.webcam.getImgp();
		    if (imgp.getDictionary().saveToFile("dictionary.dat")) {
			JOptionPane.showMessageDialog(null, "Symbols saved to file successfuly", "Dictionary",
				JOptionPane.INFORMATION_MESSAGE);
		    } else {
			JOptionPane.showMessageDialog(null, "Cannot save symbols to file", "Dictionary",
				JOptionPane.ERROR_MESSAGE);
		    }

		    updateInfo();
		}
	    }
	});

	btnClearDictionary = new JButton("Clear");
	btnClearDictionary.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int reply = JOptionPane.showConfirmDialog(null, "Are you sure to clear symbols from dictionary?",
			"Dictionary", JOptionPane.YES_NO_OPTION);

		if (reply == JOptionPane.YES_OPTION) {
		    ImageProcessor imgp = App.webcam.getImgp();
		    imgp.getDictionary().clear();

		    btnClearDictionary.setEnabled(imgp.getDictionary().getSymbols().length > 0);

		    updateInfo();
		}
	    }
	});

	btnRemove = new JButton("Remove");
	btnRemove.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		ImageProcessor imgp = App.webcam.getImgp();
		imgp.getDictionary().forget(fldCharacter.getText());

		updateInfo();

		btnRemove.setEnabled(false);
		fldCharacter.setText("");
	    }
	});
	btnRemove.setEnabled(false);
	GroupLayout gl_contentPane = new GroupLayout(contentPane);
	gl_contentPane
		.setHorizontalGroup(gl_contentPane
			.createParallelGroup(Alignment.TRAILING)
			.addGroup(
				Alignment.LEADING,
				gl_contentPane
					.createSequentialGroup()
					.addGap(28)
					.addGroup(
						gl_contentPane
							.createParallelGroup(Alignment.LEADING)
							.addGroup(
								gl_contentPane
									.createSequentialGroup()
									.addComponent(txtResult,
										GroupLayout.DEFAULT_SIZE, 400,
										Short.MAX_VALUE).addContainerGap())
							.addGroup(
								gl_contentPane
									.createSequentialGroup()
									.addGroup(
										gl_contentPane
											.createParallelGroup(
												Alignment.TRAILING,
												false)
											.addComponent(btnRemove, 0, 0,
												Short.MAX_VALUE)
											.addGroup(
												gl_contentPane
													.createParallelGroup(
														Alignment.LEADING)
													.addGroup(
														gl_contentPane
															.createParallelGroup(
																Alignment.TRAILING,
																false)
															.addComponent(
																lblCharacter,
																Alignment.LEADING,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
															.addComponent(
																lblLearning,
																Alignment.LEADING,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
															.addComponent(
																lblContour,
																Alignment.LEADING))
													.addComponent(
														lblCapturing)
													.addComponent(
														lblEquation))
											.addComponent(lblResult,
												Alignment.LEADING))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(
										gl_contentPane
											.createParallelGroup(
												Alignment.LEADING)
											.addComponent(btnAdd,
												Alignment.TRAILING, 0,
												0, Short.MAX_VALUE)
											.addGroup(
												gl_contentPane
													.createParallelGroup(
														Alignment.LEADING,
														false)
													.addComponent(
														btnLearning,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
													.addComponent(
														btnCapturing,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
													.addComponent(
														spContour,
														GroupLayout.DEFAULT_SIZE,
														82,
														Short.MAX_VALUE)
													.addComponent(
														fldCharacter,
														0,
														0,
														Short.MAX_VALUE)
													.addComponent(
														spEquation,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(
										gl_contentPane
											.createParallelGroup(
												Alignment.LEADING)
											.addComponent(lblInfo)
											.addGroup(
												gl_contentPane
													.createSequentialGroup()
													.addComponent(
														lblPreview)
													.addGap(18)
													.addComponent(
														cbPreview,
														0,
														152,
														Short.MAX_VALUE))
											.addGroup(
												gl_contentPane
													.createSequentialGroup()
													.addComponent(
														btnLoadDictionary,
														GroupLayout.PREFERRED_SIZE,
														60,
														GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(
														ComponentPlacement.RELATED)
													.addComponent(
														btnSaveDictionary,
														GroupLayout.PREFERRED_SIZE,
														72,
														GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(
														ComponentPlacement.RELATED)
													.addComponent(
														btnClearDictionary,
														GroupLayout.DEFAULT_SIZE,
														73,
														Short.MAX_VALUE))
											.addComponent(
												txtInfo,
												GroupLayout.DEFAULT_SIZE,
												217, Short.MAX_VALUE))
									.addGap(16)))));
	gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(
		gl_contentPane
			.createSequentialGroup()
			.addGroup(
				gl_contentPane
					.createParallelGroup(Alignment.BASELINE)
					.addComponent(btnCapturing)
					.addComponent(lblPreview)
					.addComponent(cbPreview, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE).addComponent(lblCapturing))
			.addPreferredGap(ComponentPlacement.RELATED)
			.addGroup(
				gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(lblLearning)
					.addComponent(btnLearning).addComponent(lblInfo))
			.addPreferredGap(ComponentPlacement.UNRELATED)
			.addGroup(
				gl_contentPane
					.createParallelGroup(Alignment.TRAILING)
					.addGroup(
						gl_contentPane
							.createSequentialGroup()
							.addGroup(
								gl_contentPane
									.createParallelGroup(Alignment.BASELINE)
									.addComponent(spEquation,
										GroupLayout.PREFERRED_SIZE, 23,
										GroupLayout.PREFERRED_SIZE)
									.addComponent(lblEquation))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(
								gl_contentPane
									.createParallelGroup(Alignment.BASELINE)
									.addComponent(lblContour)
									.addComponent(spContour,
										GroupLayout.PREFERRED_SIZE, 23,
										GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(
								gl_contentPane
									.createParallelGroup(Alignment.BASELINE)
									.addComponent(lblCharacter)
									.addComponent(fldCharacter,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)))
					.addComponent(txtInfo, GroupLayout.PREFERRED_SIZE, 95,
						GroupLayout.PREFERRED_SIZE))
			.addPreferredGap(ComponentPlacement.UNRELATED)
			.addGroup(
				gl_contentPane
					.createParallelGroup(Alignment.BASELINE)
					.addComponent(btnAdd, GroupLayout.PREFERRED_SIZE, 27,
						GroupLayout.PREFERRED_SIZE)
					.addComponent(btnLoadDictionary)
					.addComponent(btnRemove)
					.addComponent(btnClearDictionary)
					.addComponent(btnSaveDictionary, GroupLayout.PREFERRED_SIZE, 27,
						GroupLayout.PREFERRED_SIZE)).addGap(18).addComponent(lblResult)
			.addPreferredGap(ComponentPlacement.RELATED)
			.addComponent(txtResult, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
			.addContainerGap(61, Short.MAX_VALUE)));
	contentPane.setLayout(gl_contentPane);
    }

    /**
     * Update info field
     */
    public void updateInfo() {
	ImageProcessor imgp = App.webcam.getImgp();
	Symbol[] symbols = imgp.getDictionary().getSymbols();
	String info = "";

	if (btnLearning.isEnabled()) {
	    info += "Working in learn mode\n";
	} else {
	    info += "Working in capture mode\n";
	}

	info += "Symbols in dictionary (" + symbols.length + ")\n";
	if (symbols.length > 0) {
	    for (Symbol s : symbols) {
		info += s.getCharacter() + " ";
	    }
	    info += "\n";
	}

	txtInfo.setText(info);
    }

    /**
     * Update equations result
     */
    public void updateResult() {
	ImageProcessor imgp = App.webcam.getImgp();
	String res = "";

	int i = 0;
	for (Equation e : imgp.getEquations()) {
	    e.processContours(imgp.getDictionary());
	    String exp = e.getExpression();

	    res += "[" + i + "] ";
	    if (exp.isEmpty()) {
		res += "<empty>\n";
	    } else {
		res += exp + " = ";
		try {
		    res += e.getEvaluation();
		} catch (ScriptException exception) {
		    res += "<error>";
		}
		res += "\n";
	    }

	    i++;
	}

	txtResult.setText(res);
    }

    /**
     * Get current preview mode
     * 
     * @return
     */
    public Preview getPreviewMode() {
	return (Preview) cbPreview.getModel().getSelectedItem();
    }
}
