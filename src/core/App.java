package core;

import java.awt.EventQueue;

import javax.swing.UIManager;

public class App {

    public static Options options;
    public static Webcam webcam;

    public static void main(String[] args) {
	try {
	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (Throwable e) {
	    e.printStackTrace();
	}
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    options = new Options();
		    options.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});

	EventQueue.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		try {
		    webcam = new Webcam();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }
}
