package process;

import static java.lang.Math.log;
import static java.lang.Math.abs;
import static java.lang.Math.signum;

/**
 * Symbol (Hu value and character used as symbol representation)
 */
public class Symbol {

    /**
     * Estimated values
     */
    private double[] values = new double[LEVELS];

    /**
     * Tolerances
     */
    private double[] tolerances = new double[LEVELS];

    /**
     * Character representation
     */
    private String character;

    /*
     * Contour
     */
    private Contour contour;

    /**
     * Hu levels count
     */
    private final static int LEVELS = 7;

    /**
     * Mask level as unassigned using -1
     */
    public final static int UNASSIGNED = -1;

    /**
     * Constructor
     */
    public Symbol() {
	character = "";

	for (int i = 0; i < LEVELS; i++) {
	    values[i] = 0;
	    tolerances[i] = UNASSIGNED;
	}
    }

    /**
     * Constructor
     * 
     * @param values
     * @param tolerances
     * @param character
     */
    public Symbol(Contour contour, double[] values, double[] tolerances, String character) {
	this.setContour(contour);
	this.setValues(values);
	this.setTolerances(tolerances);
	this.setCharacter(character);
    }

    /**
     * Set tolerances
     * 
     * @param tolerances
     */
    public void setTolerances(double[] tolerances) {
	if (tolerances.length != LEVELS) {
	    throw new IllegalArgumentException("Invalid Hu tolerances length");
	}

	this.tolerances = tolerances;
    }

    /**
     * Set values
     * 
     * @param values
     */
    public void setValues(double[] values) {
	if (tolerances.length != LEVELS) {
	    throw new IllegalArgumentException("Invalid Hu values length");
	}

	this.values = values;
    }

    /**
     * Set value and tolerance for Hu level
     * 
     * @param level
     * @param value
     * @param tolerance
     */
    public void set(int level, double value, double tolerance) {
	this.values[level] = value;
	this.tolerances[level] = tolerance;
    }

    /**
     * Get value for Hu level
     * 
     * @param level
     * @return
     */
    public double getValue(int level) {
	return this.values[level];
    }

    public Contour getContour() {
	return contour;
    }

    public void setContour(Contour contour) {
	this.contour = contour;
    }

    /**
     * Get all values
     * 
     * @return
     */
    public double[] getValues() {
	return this.values;
    }

    /**
     * Get tolerance for Hu level
     * 
     * @param level
     * @return
     */
    public double getTolerance(int level) {
	return this.tolerances[level];
    }

    /**
     * Get tolerances
     * 
     * @return
     */
    public double[] getTolerances() {
	return this.tolerances;
    }

    /**
     * Get string representation
     * 
     * @return
     */
    public String getCharacter() {
	return character;
    }

    /**
     * @param character
     */
    public void setCharacter(String character) {
	this.character = character;
    }

    /**
     * Check value
     * 
     * @param value
     * @return
     */
    public double matchContours(double[] values) {
	double sum = 0.0;
	for (int i = 0; i < LEVELS; i++) {
	    if (this.tolerances[i] != UNASSIGNED) {
		sum += abs(1 / (signum(values[i]) * log(abs(values[i]))) - 1
			/ (signum(this.values[i]) * log(abs(this.values[i]))));
	    }
	}
	return sum;
    }

    /**
     * Data delimitery
     */
    private final static String DATA_DELIMITER = ";";
    
    /**
     * Value [ ] tolerance delimiter
     */
    private final static String VALUE_DELIMITER = " ";

    /**
     * Get value as string
     * 
     * @return
     */
    public String toString() {
	String res = this.character + DATA_DELIMITER;

	for (int i = 0; i < LEVELS; i++) {
	    res += (values[i] + VALUE_DELIMITER + tolerances[i]);
	    if (i + 1 < LEVELS) {
		res += DATA_DELIMITER;
	    }
	}

	return res;
    }

    /**
     * Set value from string
     * 
     * @param str
     */
    public void fromString(String str) {
	String s[] = str.split(DATA_DELIMITER);

	if (s.length != (1 + LEVELS)) {
	    return;
	}

	setCharacter(s[0]);

	for (int level = 0; level < LEVELS; level++) {
	    String v[] = s[1 + level].split(VALUE_DELIMITER);
	    set(level, Double.parseDouble(v[0]), Double.parseDouble(v[1]));
	}
    }
};