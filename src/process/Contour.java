package process;

import static com.googlecode.javacv.cpp.opencv_imgproc.cvBoundingRect;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvGetHuMoments;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvMoments;

import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_imgproc.CvHuMoments;
import com.googlecode.javacv.cpp.opencv_imgproc.CvMoments;

public class Contour {

    /**
     * Sequence of elements
     */
    private CvSeq seq;

    /**
     * Bounding box
     */
    private CvRect bbox;

    /**
     * Moments
     */
    private CvMoments moments;

    /**
     * Hu moments
     */
    private CvHuMoments huMoments;

    /**
     * Constructor
     * 
     * @param seq
     */
    public Contour(CvSeq seq) {
	this.setSeq(seq);
    }

    /**
     * Get contour elements sequence
     * 
     * @return
     */
    public CvSeq getSeq() {
	return seq;
    }

    /**
     * @param seq
     */
    public void setSeq(CvSeq seq) {
	this.seq = seq;
	this.bbox = cvBoundingRect(seq, 0);

	this.moments = new CvMoments();
	this.huMoments = new CvHuMoments();

	cvMoments(seq, moments, 1);
	cvGetHuMoments(moments, huMoments);
    }

    /**
     * Get bounding box
     * 
     * @return
     */
    public CvRect getBBox() {
	return this.bbox;
    }

    /**
     * Get moments
     * 
     * @return
     */
    public CvMoments getMoments() {
	return this.moments;
    }

    /**
     * Get Hu moments (set of 7 double values for shape recognition)
     * 
     * @return
     */
    public CvHuMoments getHuMoments() {
	return this.huMoments;
    }

    /**
     * Get Hu moments as double array
     * 
     * @return
     */
    public double[] getHuMomentsDouble() {
	double[] result = { huMoments.hu1(), huMoments.hu2(), huMoments.hu3(), huMoments.hu4(), huMoments.hu5(),
		huMoments.hu6(), huMoments.hu7() };
	
	return result;
    }

    /**
     * Debug contour
     */
    public void debug() {
	System.out.println("[bbox] x: " + bbox.x() + " y: " + bbox.y() + "w: " + bbox.width() + " h: " + bbox.height());
	System.out.println("[hu] 1: " + huMoments.hu1() + " 2:" + huMoments.hu2() + " 3:" + huMoments.hu3() + " 4:"
		+ huMoments.hu4() + " 5:" + huMoments.hu5() + " 6:" + huMoments.hu6() + " 7:" + huMoments.hu7());
    }
}
