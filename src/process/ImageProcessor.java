package process;

import static com.googlecode.javacpp.Loader.sizeof;
import static com.googlecode.javacv.cpp.opencv_core.CV_WHOLE_SEQ;
import static com.googlecode.javacv.cpp.opencv_core.cvCloneImage;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateMemStorage;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvPoint;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_CHAIN_APPROX_SIMPLE;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_RETR_LIST;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_THRESH_BINARY;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvContourArea;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvErode;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvFindContours;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvThreshold;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import com.googlecode.javacv.cpp.opencv_core.CvContour;
import com.googlecode.javacv.cpp.opencv_core.CvMemStorage;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

public class ImageProcessor {

    /**
     * Input image
     */
    private IplImage input;

    /**
     * Processed image
     */
    private IplImage processed;
    
    /**
     * Contured images
     */
    private IplImage contured;
    
    /**
     * Output image
     */
    private BufferedImage output;

    /**
     * Dictionary
     */
    private Dictionary dictionary;

    /**
     * All contours
     */
    private ArrayList<Contour> contours;

    /**
     * Equations
     */
    private ArrayList<Equation> equations;

    /**
     * Currently chosen contour (learning mode)
     */
    private Contour currentContour;

    /**
     * Currently chosen equation (learning mode)
     */
    private Equation currentEquation;

    /**
     * Constructor
     */
    public ImageProcessor() {
	currentContour = null;
	currentEquation = null;
	this.contours = new ArrayList<Contour>();
	this.equations = new ArrayList<Equation>();
	this.dictionary = new Dictionary();
    }

    /**
     * Reset processing data
     */
    public void reset() {
	this.contours.clear();
	this.equations.clear();
	
	this.processed = null;
	this.contured = null;
    }

    /**
     * Get input image
     * 
     * @return
     */
    public BufferedImage getInput() {
	return this.input.getBufferedImage();
    }
    
    /**
     * Get processed image
     * 
     * @return
     */
    public BufferedImage getProcessed() {
	return this.processed.getBufferedImage();
    }
    
    /**
     * Get contured image
     * 
     * @return
     */
    public BufferedImage getContured() {
	return this.contured.getBufferedImage();
    }

    /**
     * Get output image
     * 
     * @return
     */
    public BufferedImage getOutput() {
	return this.output;
    }

    /**
     * Get used dictionary
     * 
     * @return
     */
    public Dictionary getDictionary() {
	return this.dictionary;
    }

    public Contour getCurrentContour() {
	return currentContour;
    }

    public void setCurrentContour(Contour currentContour) {
	this.currentContour = currentContour;
    }

    public Equation getCurrentEquation() {
	return currentEquation;
    }

    public void setCurrentEquation(Equation currentEquation) {
	this.currentEquation = currentEquation;
    }

    /**
     * Initialize image processing
     * 
     * @param img
     */
    public void init(IplImage img) {
	this.input = img;
	this.output = img.getBufferedImage();
    }
    
    /**
     * Process image
     * 
     * @param input
     * @return
     */
    public void process() {
	processed = cvCreateImage(cvGetSize(input), 8, CV_THRESH_BINARY);
	cvCvtColor(input, processed, CV_BGR2GRAY);
	cvThreshold(processed, processed, 100, 255, CV_THRESH_BINARY);
	cvErode(processed, processed, null, 1);

	findValidContours(50.0, 70);
	findEquations(20);
    }

    private boolean isInsideROI(CvRect rect, CvRect ROI) {
	if ((rect.x() >= ROI.x()) && (rect.y() >= ROI.y()) && (rect.x() + rect.width() <= ROI.x() + ROI.width())
		&& (rect.y() + rect.height() <= ROI.y() + ROI.height())) {
	    return true;
	} else {
	    return false;
	}
    }

    private void findValidContours(double minArea, int paperRatio) {
	contured = cvCloneImage(processed);
	
	CvMemStorage mem = cvCreateMemStorage(0);
	CvSeq contours = new CvSeq();
	CvSeq ptr = new CvSeq();
	double contourArea = 0;

	// ROI
	int imageWidth = contured.width();
	int imageHeight = contured.height();

	int roiWidth = (int) (imageWidth * paperRatio / 100);
	int roiHeight = (int) (imageHeight * paperRatio / 100);

	int dw = (int) (imageWidth - roiWidth) / 2;
	int dh = (int) (imageHeight - roiHeight) / 2;

	CvRect paper = new CvRect(dw, dh, roiWidth, roiHeight);

	cvFindContours(contured, mem, contours, sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE,
		cvPoint(0, 0));

	for (ptr = contours; ptr != null; ptr = ptr.h_next()) {
	    contourArea = cvContourArea(ptr, CV_WHOLE_SEQ, 0);
	    if (contourArea > minArea) {
		Contour contour = new Contour(ptr);
		if (isInsideROI(contour.getBBox(), paper)) {
		    this.contours.add(contour);
		}
	    }
	}
    }

    private void findEquations(int tolerance) {
	boolean found = false;
	// Sort by x position
	Collections.sort(this.contours, new Comparator<Contour>() {
	    public int compare(Contour cont1, Contour cont2) {
		return cont1.getBBox().x() - cont2.getBBox().x();
	    }
	});
	// Remove interior contours
	if (this.contours.size() >= 2) {
	    Iterator<Contour> it2 = this.contours.iterator();
	    Contour prev = (Contour) it2.next();
	    while (it2.hasNext()) {
		Contour c = (Contour) it2.next();
		if (isInsideROI(c.getBBox(), prev.getBBox())) {
		    it2.remove();
		} else {
		    prev = c;
		}
	    }
	}
	// Grouping contours into equations
	for (Contour c : this.contours) {
	    if (this.equations.isEmpty()) {
		Equation equation = new Equation(dictionary);
		equation.addContour(c);
		this.equations.add(equation);
	    } else {
		found = false;
		for (Equation e : this.equations) {
		    if (e.isInEquation(c, tolerance)) {
			found = true;
			e.addContour(c);
			break;
		    }
		}
		if (!found) {
		    Equation equation = new Equation(dictionary);
		    equation.addContour(c);
		    this.equations.add(equation);
		}
	    }
	}
	// Remove invalid equations (equation - minimum of 3 contours)
	ArrayList<Equation> invalidE = new ArrayList<Equation>();
	
	for (Equation e : getEquations()) {
	    if (e.getSize() < 3) {
		this.equations.remove(e);
	    }
	    
	    // @fixme Duplicated operation of deleting internal contours, but
	    // works incorrectly without it
	    Contour previous = null;
	    for (Contour c : e.getContours()) {
		if (previous != null) {
		   if (isInsideROI(c.getBBox(), previous.getBBox())) {
		       e.removeContour(c);
		   }
		}
		previous = c;
	    }
	}
	
	this.equations.removeAll(invalidE);
    }

    /**
     * Debug processing
     * 
     * @return
     */
    public void debug() {
	for (int i = 0; i < contours.size(); i++) {
	    Contour contour = contours.get(i);
	    contour.debug();
	}
    }

    /**
     * Draw processing visualization
     * 
     * @return
     */
    public void drawAll() {
	Graphics g = output.getGraphics();

	for (int i = 0; i < equations.size(); i++) {
	    Equation e = equations.get(i);

	    // First draw contours in equation
	    for (Contour c : e.getContours()) {
		drawContour(c);
	    }

	    // After that draw whole equation
	    drawEquation(e);
	}
    }

    /**
     * Draw contour
     * 
     * @param c
     */
    public void drawContour(Contour c) {
	Graphics g = output.getGraphics();
	g.setColor(new Color(255, 0, 0));
	g.drawRect(c.getBBox().x(), c.getBBox().y(), c.getBBox().width(), c.getBBox().height());
    }

    public void drawCurrentContour() {
	if (this.currentContour != null) {
	    drawContour(this.currentContour);
	}
    }

    /**
     * Draw equation
     * 
     * @param e
     */
    public void drawEquation(Equation e) {
	Graphics g = output.getGraphics();
	g.setColor(new Color(0, 255, 0));
	g.drawRect(e.getBBox().x(), e.getBBox().y(), e.getBBox().width(), e.getBBox().height());
    }

    public void drawCurrentEquation() {
	if (this.currentEquation != null) {
	    drawEquation(this.currentEquation);
	}
    }

    /**
     * Get recognized equations
     * 
     * @return
     */
    public Equation[] getEquations() {
	return this.equations.toArray(new Equation[this.equations.size()]);
    }
}
