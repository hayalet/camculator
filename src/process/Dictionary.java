package process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static com.googlecode.javacv.cpp.opencv_imgproc.cvMatchShapes;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_CONTOURS_MATCH_I1;

public class Dictionary {

    /**
     * Symbols for matching symbols
     */
    private ArrayList<Symbol> symbols;

    /**
     * Constructor
     */
    public Dictionary() {
	this.symbols = new ArrayList<Symbol>();
    }

    /**
     * Check whether character is already known
     * 
     * @param character
     * @return
     */
    public boolean has(String character) {
	for (Symbol w : symbols) {
	    if (w.getCharacter().equals(character)) {
		return true;
	    }
	}

	return false;
    }
    
    public boolean forget(String character) {
	for (Symbol s: getSymbols()) {
	    if (s.getCharacter().equals(character)) {
		this.symbols.remove(s);
		return true;
	    }
	}
	
	return false;
    }

    /**
     * Read symbol
     * 
     * @return
     */
    public Symbol read(Contour contour) {
	double bestSum = 100.0;
	Symbol bestWord = null;
	for (Symbol word : symbols) {
	    double sum = word.matchContours(contour.getHuMomentsDouble());
	    if (sum < bestSum) {
		bestSum = sum;
		bestWord = word;
	    }
	}

	return bestWord;
    }

    /**
     * Learn word
     * 
     * @param contour
     * @param character
     * @return
     */
    public Symbol learn(Contour contour, String character) {
	double[] tolerances = { 0.01, 0.01, 1E-5, 1E-5, 1E-10, 1E-10, 1E-15 };
	double[] values = contour.getHuMomentsDouble();

	Symbol word = new Symbol(contour, values, tolerances, character);
	symbols.add(word);

	return word;
    }

    /**
     * Load symbols from file
     * 
     * @param file
     */
    public boolean loadFromFile(String file) {
	symbols.clear();

	FileReader handle;
	BufferedReader in;

	try {
	    handle = new FileReader(file);
	    in = new BufferedReader(handle);
	    String line;

	    while ((line = in.readLine()) != null) {
		Symbol s = new Symbol();
		s.fromString(line);
		symbols.add(s);
	    }
	    
	    in.close();
	    handle.close();
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    return false;
	} catch (IOException e) {
	    e.printStackTrace();
	    return false;
	}

	return true;
    }

    /**
     * Save current symbols to file
     * 
     * @param file
     */
    public boolean saveToFile(String file) {
	FileWriter handle;
	BufferedWriter out;

	try {
	    handle = new FileWriter(file);
	    out = new BufferedWriter(handle);

	    for (Symbol s : symbols) {
		out.write(s.toString());
		out.newLine();
	    }
	    
	    out.close();
	    handle.close();
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    return false;
	} catch (IOException e) {
	    e.printStackTrace();
	    return false;
	}

	return true;
    }

    /**
     * Get symbols
     * 
     * @return
     */
    public Symbol[] getSymbols() {
	return this.symbols.toArray(new Symbol[this.symbols.size()]);
    }

    public void clear() {
	this.symbols.clear();
    }

}
