package process;

import java.util.ArrayList;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.googlecode.javacv.cpp.opencv_core.CvRect;

public class Equation {

    private ArrayList<Contour> contours;
    private ArrayList<Symbol> symbols;

    private CvRect bbox;

    /**
     * Constructor
     */
    public Equation(Dictionary dictionary) {
	this.contours = new ArrayList<Contour>();
	this.symbols = new ArrayList<Symbol>();
	this.bbox = new CvRect(0, 480, 0, 0);
    }

    /**
     * @return bounding box
     */
    public CvRect getBBox() {
	return this.bbox;
    }

    /**
     * @return number of contours in equation
     */
    public int getSize() {
	return this.contours.size();
    }

    /**
     * @return array of contours
     */
    public Contour[] getContours() {
	return this.contours.toArray(new Contour[this.contours.size()]);
    }

    /**
     * @return array of symbols
     */
    public Symbol[] getSymbols() {
	return this.symbols.toArray(new Symbol[this.symbols.size()]);
    }

    /**
     * Adds contour to equation, if first, sets equation bounding box x, y,
     * width, height else if needed changes width, y and height to include all
     * contours
     * 
     * @param contour
     */

    public void addContour(Contour contour) {
	CvRect contBBox = contour.getBBox();
	if (this.contours.isEmpty()) {
	    this.bbox.x(contBBox.x());
	    this.bbox.width(contBBox.width());
	} else {
	    this.bbox.width(contBBox.x() + contBBox.width() - this.bbox.x());
	}
	if (contBBox.y() < this.bbox.y()) {
	    this.bbox.y(contBBox.y());
	}
	if (contBBox.height() > this.bbox.height()) {
	    this.bbox.height(contBBox.height());
	}
	this.contours.add(contour);
    }

    /**
     * Tests if contour belong to equation, there are 2 scenarios: 1) Contour is
     * fully inside the equation bbox - y >= e.y and y+height <= e.y + e.height
     * 2) Contour is outside the equation bbox but offset is not greater than
     * tolerance
     * 
     * @param contour
     * @param tolerance
     * @return if contours belongs to equation
     */
    public boolean isInEquation(final Contour contour, int tolerance) {
	CvRect contBBox = contour.getBBox();
	if ((contBBox.y() >= this.bbox.y() && contBBox.y() + contBBox.height() <= this.bbox.y() + this.bbox.height())
		|| (this.bbox.y() - tolerance <= contBBox.y() && this.bbox.y() + this.bbox.height() + tolerance >= contBBox
			.y() + contBBox.height())) {
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Process contours, get symbols using dictionary
     */
    public void processContours(Dictionary dictionary) {
	symbols.clear();

	for (Contour contour : contours) {
	    Symbol symbol = dictionary.read(contour);

	    if (symbol != null) {
		symbols.add(symbol);
	    }
	}
    }

    /**
     * Get expression
     * 
     * @return
     */
    public String getExpression() {
	String exp = "";

	for (Symbol symbol : this.symbols) {
	    exp += symbol.getCharacter();
	}

	return exp;
    }

    /**
     * Get evaluation
     * 
     * @return
     */
    public String getEvaluation() throws ScriptException {
	ScriptEngineManager mgr = new ScriptEngineManager();
	ScriptEngine engine = mgr.getEngineByName("JavaScript");
	return engine.eval(getExpression()).toString();
    }

    /**
     * Remove contour
     * 
     * @param contour
     */
    public void removeContour(Contour contour) {
	this.contours.remove(contour);
    }
}
